//
//  FlashcardsTableViewController.swift
//  PortableFL
//
//  Created by Tinkertanker on 12/12/19.
//  Copyright © 2019 Tinkertanker. All rights reserved.
//

import UIKit

class FlashcardsTableViewController: UITableViewController {
    var flashcards: [Flashcard]! = Flashcard.loadFromFile() ?? []
    var selectedIndex: Int!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return flashcards.count
    }
    
    @IBAction func unwindToEditScreen(for unwindSegue: UIStoryboardSegue, towards subsequentVC: UIViewController) {
        // Reload data
        flashcards = Flashcard.loadFromFile() ?? []
        tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "flashcardCell", for: indexPath)
        cell.textLabel!.text = flashcards[indexPath.row].title
        cell.detailTextLabel!.text = flashcards[indexPath.row].definition
        // Configure the cell...
        
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        performSegue(withIdentifier: "editFlashcard", sender: self)
        
    }
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            /* MARK: Deleting images using FileManager */
            let imageURL = flashcards[indexPath.row].imageURL
            let fm = FileManager()
            
            // We can force this to run because we know the image exists
            try! fm.removeItem(at: imageURL)
            
            
            flashcards.remove(at: indexPath.row)
            Flashcard.saveToFile(flashcards: flashcards)
            
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
            
            tableView.reloadData()
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "editFlashcard" {
            let dest = segue.destination as! EditFlashcardTableViewController
            dest.editedFlashcard = flashcards[selectedIndex]
            dest.editedFlashcardIndex = selectedIndex
            dest.isAdding = false
        } else if segue.identifier == "addFlashcard" {
            let dest = segue.destination as! EditFlashcardTableViewController
            dest.isAdding = true
        }
    }
    
    
}
