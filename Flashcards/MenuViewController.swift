//
//  MenuViewController.swift
//  PortableFL
//
//  Created by Tinkertanker on 12/12/19.
//  Copyright © 2019 Tinkertanker. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var viewButton: UIButton!
    var flashcards = Flashcard.loadFromFile() ?? []
    override func viewDidLoad() {
        super.viewDidLoad()
        viewButton.setTitleColor(.gray, for: .disabled)
        // Do any additional setup after loading the view.
        if flashcards.count == 0 {
            viewButton.isEnabled = false
        }
    }
    
    @IBAction func unwindToMenu(for unwindSegue: UIStoryboardSegue, towards subsequentVC: UIViewController) {
        flashcards = Flashcard.loadFromFile() ?? []
        if flashcards.count == 0 {
            viewButton.isEnabled = false
        } else {
            viewButton.isEnabled = true
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "viewFlashcards" {
            let dest = segue.destination as! FlashcardViewController
            let flashcards = Flashcard.loadFromFile()!
            dest.flashcards = flashcards
        }
    }
}

