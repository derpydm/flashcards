//
//  ImagePicker.swift
//  PortableFL
//
//  Created by Tinkertanker on 16/12/19.
//  Copyright © 2019 Tinkertanker. All rights reserved.
//

import Foundation

import UIKit

public protocol ImagePickerDelegate: class {
    func didSelect(image: UIImage?, imageURL: URL?)
}

open class ImagePicker: NSObject {

    private let pickerController: UIImagePickerController
    private weak var presentationController: UIViewController?
    private weak var delegate: ImagePickerDelegate?

    public init(presentationController: UIViewController, delegate: ImagePickerDelegate) {
        self.pickerController = UIImagePickerController()

        super.init()

        self.presentationController = presentationController
        self.delegate = delegate
    
        self.pickerController.delegate = self
        self.pickerController.allowsEditing = true
        self.pickerController.mediaTypes = ["public.image"]
    }
    
    private func action(for type: UIImagePickerController.SourceType, title: String) -> UIAlertAction? {
        guard UIImagePickerController.isSourceTypeAvailable(type) else {
            return nil
        }
        
        return UIAlertAction(title: title, style: .default) { _ in
            self.pickerController.sourceType = type
            self.presentationController?.present(self.pickerController, animated: true)
        }
    }
    
    public func present(from sourceView: UIView) {

        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        if let action = self.action(for: .photoLibrary, title: "Photo Library") {
            alertController.addAction(action)
        }
        
        if let action = self.action(for: .camera, title: "Camera") {
            alertController.addAction(action)
        }
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        if UIDevice.current.userInterfaceIdiom == .pad {
            alertController.popoverPresentationController?.sourceView = sourceView
            alertController.popoverPresentationController?.sourceRect = sourceView.bounds
            alertController.popoverPresentationController?.permittedArrowDirections = [.down, .up]
        }

        self.presentationController?.present(alertController, animated: true)
    }
    
    private func pickerController(_ controller: UIImagePickerController, didSelect image: UIImage?, withURL url: URL?) {
        self.delegate?.didSelect(image: image, imageURL: url)
        controller.dismiss(animated: true, completion: nil)
       
    }
}

extension ImagePicker: UIImagePickerControllerDelegate {
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("cancelled")
        self.pickerController(picker, didSelect: nil, withURL: nil)
    }

    public func imagePickerController(_ picker: UIImagePickerController,
                                      didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        print("is this function even called? wtf:9")
        guard let image = info[.editedImage] as? UIImage else {
            print("Missing Image")
            return self.pickerController(picker, didSelect: nil, withURL: nil)
        }
        guard let url = info[.imageURL] as? URL else {
            print("Missing URL")
            return self.pickerController(picker, didSelect: nil, withURL: nil)
        }
        self.pickerController(picker, didSelect: image, withURL: url)
    }
}

extension ImagePicker: UINavigationControllerDelegate {
    
}
