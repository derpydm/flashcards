//
//  EditFlashcardViewControllerTableViewController.swift
//  PortableFL
//
//  Created by Tinkertanker on 12/12/19.
//  Copyright © 2019 Tinkertanker. All rights reserved.
//

import UIKit

class EditFlashcardTableViewController: UITableViewController {
    
    
    
    var selectedImage: UIImage!
    var imageLink: URL!
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var exampleSentenceTextField: UITextField!
    @IBOutlet weak var definitionTextField: UITextField!
    @IBOutlet weak var titleTextField: UITextField!
    var isAdding = true
    var editedFlashcard: Flashcard!
    var editedFlashcardIndex: Int!
    var flashcards: [Flashcard]!
    var imagePicker: ImagePicker!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /* MARK: Set up Image Picker*/
        
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
        
        flashcards = Flashcard.loadFromFile()
        if !(isAdding) {
            titleTextField.text = editedFlashcard.title
            definitionTextField.text = editedFlashcard.definition
            exampleSentenceTextField.text = editedFlashcard.exampleSentence
            navBar.topItem?.title = "Edit Flashcard"
            imageLink = editedFlashcard.imageURL
            let data = try! Data(contentsOf: imageLink)
            selectedImage = UIImage(data: data)
        }
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    @IBAction func pickImage(_ sender: Any) {
        imagePicker.present(from: self.view)
    }
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 4
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }
    
    @IBAction func onSave(_ sender: Any) {
        if isAdding {
            let title = titleTextField.text ?? ""
            if title == "" {
                let alert = UIAlertController(title: "Missing Data", message: "Please enter a title in!", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(alert, animated: true)
                return
            }
            
            
            let desc = definitionTextField.text ?? ""
            if desc == "" {
                let alert = UIAlertController(title: "Missing Data", message: "Please enter a definition in!", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(alert, animated: true)
                return
            }
            
            
            let exampleSent = exampleSentenceTextField.text ?? ""
            if exampleSent == "" {
                let alert = UIAlertController(title: "Missing Data", message: "Please enter an example sentence in!", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(alert, animated: true)
                return
            }
            guard let img = selectedImage else {
                let alert = UIAlertController(title: "Missing Data", message: "Please provide an image!", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(alert, animated: true)
                return
            }
            
            /* MARK: Storing Images */
            
            // Our name for each image is just an id
            // It will be incremented by 1 every time a new image is stored
            
            let defaults = UserDefaults.standard
            var currentImgNo = defaults.integer(forKey: "imageName")
            currentImgNo += 1
            defaults.set(currentImgNo, forKey: "imageName")
            
            // Save image to png
            
            let imageName = String(currentImgNo)
            let imagePath: String = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(imageName).png"
            let imageUrl: URL = URL(fileURLWithPath: imagePath)
            
            try? selectedImage.pngData()?.write(to: imageUrl)
            
            // Create the new flashcard and store it
            
            let newFlashcard = Flashcard(title: title, description: desc, exampleSentence: exampleSent, imageURL: imageUrl)
            var flashcards = Flashcard.loadFromFile() ?? []
            flashcards.append(newFlashcard)
            Flashcard.saveToFile(flashcards: flashcards)
            
            // Go back to edit menu
            
            performSegue(withIdentifier: "unwindToEdit", sender: self)
            
        } else {
            
            let title = titleTextField.text ?? ""
            if title == "" {
                let alert = UIAlertController(title: "Missing Data", message: "Please enter a title in!", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(alert, animated: true)
                return
            }
            
            
            let desc = definitionTextField.text ?? ""
            if desc == "" {
                let alert = UIAlertController(title: "Missing Data", message: "Please enter a definition in!", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(alert, animated: true)
                return
            }
            
            
            let exampleSent = exampleSentenceTextField.text ?? ""
            if exampleSent == "" {
                let alert = UIAlertController(title: "Missing Data", message: "Please enter an example sentence in!", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(alert, animated: true)
                return
            }
            
            
            let newFlashcard = Flashcard(title: title, description: desc, exampleSentence: exampleSent, imageURL: imageLink)
            var flashcards = Flashcard.loadFromFile() ?? []
            flashcards[editedFlashcardIndex] = newFlashcard
            Flashcard.saveToFile(flashcards: flashcards)
            performSegue(withIdentifier: "unwindToEdit", sender: self)
        }
    }
    /*
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
     
     // Configure the cell...
     
     return cell
     }
     */
    

     // Override to support conditional editing of the table view.
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension EditFlashcardTableViewController: ImagePickerDelegate {
    func didSelect(image: UIImage?, imageURL: URL?) {
        self.imageLink = imageURL
        self.selectedImage = image
    }
}
